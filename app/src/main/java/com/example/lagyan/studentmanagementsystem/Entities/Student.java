package com.example.lagyan.studentmanagementsystem.Entities;

import java.io.Serializable;

/**
 * Created by LAGYAN on 9/7/2015.
 */
public class Student implements Serializable
{
    public int rollNo;
    public String studentName,address,mobile;

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


}
