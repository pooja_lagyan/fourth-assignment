package com.example.lagyan.studentmanagementsystem.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lagyan.studentmanagementsystem.Comparators.CompareByName;
import com.example.lagyan.studentmanagementsystem.Comparators.CompareByRollNO;
import com.example.lagyan.studentmanagementsystem.Entities.Alert_Dialog;
import com.example.lagyan.studentmanagementsystem.Entities.MyAdapter;
import com.example.lagyan.studentmanagementsystem.Entities.Student;
import com.example.lagyan.studentmanagementsystem.R;
import com.example.lagyan.studentmanagementsystem.util.AddStudentData;
import com.example.lagyan.studentmanagementsystem.util.Constants;
import com.example.lagyan.studentmanagementsystem.util.MyDialogListener;

import java.util.Collections;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,Constants,MyDialogListener {

    MyAdapter adapter;
    Intent intent;
    public static int studentId = 1;
    LinkedList<Student> studentsList;
    int positionOfItem;
    GridView gridView;
    ListView listView;
    TextView message;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button listButton = (Button)findViewById(R.id.listButton);
        Button gridButton = (Button)findViewById(R.id.gridButton);
        listButton.setOnClickListener(this);
        gridButton.setOnClickListener(this);
        message = (TextView)findViewById(R.id.message);
        studentsList = new LinkedList<>();
        adapter = new MyAdapter(this , studentsList , R.layout.row_layout);

        gridView = (GridView)findViewById(R.id.gridView);
        listView= (ListView)findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alert_Dialog dialogObj = new Alert_Dialog();
                dialogObj.setDialogListener(MainActivity.this);
                positionOfItem = position;
                dialogObj.show(getFragmentManager(), DIALOG_KEY);
            }
        });

        spinner = (Spinner)findViewById(R.id.spinner);

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,getResources().getStringArray(R.array.spinnerList));
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sort = spinner.getSelectedItem().toString();
                if (sort.equals(SORT_ROLLNO_KEY)) {
                    Collections.sort(studentsList, new CompareByRollNO());
                    adapter.notifyDataSetChanged();
                } else if (sort.equals(SORT_NAME_KEY)) {
                    Collections.sort(studentsList, new CompareByName());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button recieveButton = (Button)findViewById(R.id.recieveButton);
        recieveButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.recieveButton:
                intent = new Intent(MainActivity.this,AddStudentData.class);
                startActivityForResult(intent,ADD_DETAILS_CODE);
                break;

            case R.id.listButton :
                listView.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.INVISIBLE);

                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Alert_Dialog dialogObj = new Alert_Dialog();
                        dialogObj.setDialogListener(MainActivity.this);
                        positionOfItem = position;
                        dialogObj.show(getFragmentManager(), DIALOG_KEY);
                    }
                });
                break;

            case R.id.gridButton :
                listView.setVisibility(View.INVISIBLE);
                gridView.setVisibility(View.VISIBLE);

                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Alert_Dialog dialogObj = new Alert_Dialog();
                        dialogObj.setDialogListener(MainActivity.this);
                        positionOfItem = position;
                        dialogObj.show(getFragmentManager(), DIALOG_KEY);
                    }
                });
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Student studentObj = new Student();

        if(resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case ADD_DETAILS_CODE:
                    studentObj.rollNo = studentId++;
                    studentObj.studentName=data.getStringExtra(NAME_KEY);
                    studentObj.address=data.getStringExtra(ADDRESS_KEY);
                    studentObj.mobile=data.getStringExtra(MOBILE_KEY);
                    studentsList.add(studentObj);
                    if(spinner.getSelectedItem().toString().equals(SORT_NAME_KEY))
                    {
                        Collections.sort(studentsList,new CompareByName());
                    }
                    adapter.notifyDataSetChanged();
                    message.setText(studentObj.getStudentName()+ADD_DETAIL_KEY);
                    break;

                case EDIT_REQUEST_CODE :
                    studentsList.set(positionOfItem, (Student) data.getSerializableExtra(STUDENT_KEY));
                    studentObj.studentName=data.getStringExtra(NAME_KEY);
                    message.setText(studentObj.getStudentName() + UPDATE_DETAIL_KEY);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
        else
        {
            message.setText("");
        }
    }
    @Override
    public void onClick(int position) {
        switch (position) {
            case EDIT:
                Intent intent = new Intent(this, AddStudentData.class);
                intent.putExtra(STUDENT_KEY , studentsList.get(positionOfItem));
                intent.putExtra(IS_EDITABLE_KEY, true);
                startActivityForResult(intent, EDIT_REQUEST_CODE);
                break;

            case  DISPLAY :
                Intent intent1 = new Intent(this,AddStudentData.class);
                intent1.putExtra(STUDENT_KEY , studentsList.get(positionOfItem));
                intent1.putExtra(IS_EDITABLE_KEY,false);
                message.setText("");
                startActivity(intent1);
                break;

            case DELETE :
                studentsList.remove(positionOfItem);
                adapter.notifyDataSetChanged();
                message.setText("");
                Toast.makeText(this,DATA_DELETED,Toast.LENGTH_SHORT).show();
                break;
        }
    }

}
