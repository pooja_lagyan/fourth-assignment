package com.example.lagyan.studentmanagementsystem.util;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lagyan.studentmanagementsystem.Entities.Student;
import com.example.lagyan.studentmanagementsystem.main.MainActivity;
import com.example.lagyan.studentmanagementsystem.R;

public class AddStudentData extends AppCompatActivity implements Constants, View.OnClickListener {

    Student student;
    boolean isEditable;
    Button saveButton;
    TextView enterdataTextView;
    EditText nameText = null,addressText =null,mobileText=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_data);
        saveButton = (Button)findViewById(R.id.saveButton);
        enterdataTextView = (TextView)findViewById(R.id.enterdataText);

        if (getIntent().hasExtra(STUDENT_KEY)) {
            student = (Student)getIntent().getSerializableExtra(STUDENT_KEY);
            isEditable = getIntent().getBooleanExtra(IS_EDITABLE_KEY, true);
        }

        nameText = (EditText)findViewById(R.id.nameText);
        addressText = (EditText)findViewById(R.id.addressText);
        mobileText = (EditText)findViewById(R.id.mobileText);
        if(student == null)
        {
            enterdataTextView.setText(ENTER_DATA_TEXT + MainActivity.studentId);

        }
        if(student != null)
        {
            nameText.setText(student.getStudentName());
            mobileText.setText(student.getMobile());
            addressText.setText(student.getAddress());
            nameText.setEnabled(isEditable);
            addressText.setEnabled(isEditable);
            mobileText.setEnabled(isEditable);
            if(!isEditable)
            {
                enterdataTextView.setText(DISPLAY_DATA_TEXT+student.getRollNo());
                saveButton.setVisibility(View.INVISIBLE);
                nameText.setTextColor(Color.rgb(0, 0, 0));
                addressText.setTextColor(Color.rgb(0, 0, 0));
                mobileText.setTextColor(Color.rgb(0, 0, 0));
            }
            else
            {
                enterdataTextView.setText(EDIT_DATA_TEXT+ student.getRollNo());
                saveButton.setVisibility(View.VISIBLE);
            }
        }
        final Button saveButton = (Button)findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);
        final Button cancelButton = (Button)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.saveButton :
                String name = nameText.getText().toString();
                String address = addressText.getText().toString();
                String mobile = mobileText.getText().toString();
                if(!name.isEmpty() && !address.isEmpty() && !mobile.isEmpty() && mobile.length()==10)
                {
                    Intent intent = new Intent();
                    if (isEditable)
                    {
                        student.setStudentName(name);
                        student.setAddress(address);
                        student.setMobile(mobile);
                        intent.putExtra(STUDENT_KEY, student);
                    }
                    intent.putExtra(NAME_KEY,name);
                    intent.putExtra(MOBILE_KEY,mobile);
                    intent.putExtra(ADDRESS_KEY,address);
                    setResult(RESULT_OK, intent);
                    finish();
                    }
                else if(name.isEmpty() || address.isEmpty() || mobile.isEmpty())
                {
                    Toast.makeText(this,ENTER_DATA_KEY,Toast.LENGTH_SHORT).show();
                }
                else if(mobile.length() < 10)
                {
                    Toast.makeText(this,ERROR_KEY,Toast.LENGTH_SHORT).show();
                }
                break;

            case  R.id.cancelButton :

                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
