package com.example.lagyan.studentmanagementsystem.Comparators;

import com.example.lagyan.studentmanagementsystem.Entities.Student;

import java.util.Comparator;

/**
 * Created by LAGYAN on 9/9/2015.
 */
public class CompareByName implements Comparator<Student>{
    @Override
    public int compare(Student lhs, Student rhs)
    {
        String firstName = lhs.getStudentName();
        String secondName = rhs.getStudentName();
        int compareResult = firstName.compareToIgnoreCase(secondName);
        if(compareResult >0)
            return 1;
        else
            return -1;

    }
}
