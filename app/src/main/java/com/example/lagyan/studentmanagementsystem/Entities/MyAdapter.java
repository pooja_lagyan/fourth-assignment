package com.example.lagyan.studentmanagementsystem.Entities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lagyan.studentmanagementsystem.Entities.Student;
import com.example.lagyan.studentmanagementsystem.R;

import java.util.LinkedList;

/**
 * Created by LAGYAN on 9/4/2015.
 */
public class MyAdapter extends BaseAdapter{

    LinkedList<Student> students = new LinkedList<Student>();
    Context context;
    int rowLayout;

    public MyAdapter(Context context, LinkedList<Student> students, int rowLayout) {
        this.context = context;
        this.students = students;
        this.rowLayout = rowLayout;
    }

    @Override
    public int getCount() {
        return students==null ? 0 : students.size();
    }

    @Override
    public Object getItem(int position) {
        return students.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(context , rowLayout , null);

        TextView name = (TextView)convertView.findViewById(R.id.name);
        TextView rollNo = (TextView)convertView.findViewById(R.id.rollNo);
        TextView address = (TextView)convertView.findViewById(R.id.address);
        TextView mobile = (TextView)convertView.findViewById(R.id.mobile);

        name.setText(students.get(position).getStudentName());
        rollNo.setText(String.valueOf(students.get(position).getRollNo()));
        address.setText(students.get(position).getAddress());
        mobile.setText(students.get(position).getMobile());

        return convertView;
    }
}
