package com.example.lagyan.studentmanagementsystem.util;

/**
 * Created by LAGYAN on 9/8/2015.
 */
public interface Constants
{
    int EDIT = 0;
    int DISPLAY = 1;
    int DELETE = 2;

    int ADD_DETAILS_CODE = 1;
    int EDIT_REQUEST_CODE = 100;

    String STUDENT_KEY = "Student";
    String IS_EDITABLE_KEY = "is Editable";
    String NAME_KEY = "name";
    String ADDRESS_KEY = "address";
    String MOBILE_KEY = "mobile";
    String DATA_DELETED = "Data deleted";

    String DIALOG_KEY = "my_dialog";
    String SORT_ROLLNO_KEY = "Roll No";
    String SORT_NAME_KEY = "Name";

    String ADD_DETAIL_KEY = "'s details added";
    String UPDATE_DETAIL_KEY = "'s details updated";
    String ENTER_DATA_TEXT = "Enter the data of ID : ";

    String ENTER_DATA_KEY = "enter the complete data";
    String ERROR_KEY = "Enter 10 digit mobile number";
    String EDIT_DATA_TEXT = "Edit the data of ID : ";
    String DISPLAY_DATA_TEXT ="Data of ID : ";
}

