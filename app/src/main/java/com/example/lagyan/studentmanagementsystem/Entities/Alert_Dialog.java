package com.example.lagyan.studentmanagementsystem.Entities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.lagyan.studentmanagementsystem.util.Constants;
import com.example.lagyan.studentmanagementsystem.util.MyDialogListener;
import com.example.lagyan.studentmanagementsystem.R;

/**
 * Created by LAGYAN on 9/8/2015.
 */
public class Alert_Dialog extends DialogFragment implements View.OnClickListener,Constants {

    LayoutInflater inflater;
    View view;
    MyDialogListener dialogListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        inflater =getActivity().getLayoutInflater();
        view=inflater.inflate(R.layout.dialog_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Button edit = (Button)view.findViewById(R.id.edit);
        Button display = (Button)view.findViewById(R.id.display);
        Button delete = (Button)view.findViewById(R.id.delete);
        edit.setOnClickListener(this);
        delete.setOnClickListener(this);
        display.setOnClickListener(this);
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.edit :
                dialogListener.onClick(EDIT);
                dismiss();
                break;

            case R.id.display :
                dialogListener.onClick(DISPLAY);
                dismiss();
                break;

            case R.id.delete :
                dialogListener.onClick(DELETE);
                dismiss();
                break;
        }
    }

    public void setDialogListener(MyDialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }
}


